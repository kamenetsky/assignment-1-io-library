section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ;запись кода системного вызова
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1     
    mov rsi, rsp 
    add rsp, 8
    mov rax, 1      
    mov rdi, 1      
    syscall         
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    sub rsp, 24
    mov byte[rdi], 0
.loop:
    xor rdx, rdx
    div rsi
    or dl, 48
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
cmp rdi,0
jnl print_uint   
push rdi 
mov rdi, '-' 
call print_char
pop rdi
neg rdi 
jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
xor rcx, rcx
xor rax, rax
.loop:
    mov dl, byte [rdi + rcx]
    cmp dl, byte [rsi + rcx]
    jne .not
    inc rcx
    test dl, dl
    jne .loop
    inc rax
.not:
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push rax
    mov rsi, rsp
    syscall
    cmp rax, -1
    jne .yes
    xor rax, rax
    add rsp, 8
    ret
    .yes:
        pop rax
        ret 




; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
push r12
push r13
push r14
push rdi
mov r12, rdi
mov r13, rsi
mov r14, rcx
.loopa:
    call read_char
    cmp rax, ` `
    je .loopa
    cmp rax, `\t`
    je .loopa
    cmp rax, `\n`
    je .loopa
    xor rcx, rcx


.loop:
    inc r14
    cmp r14, r13
    jg .overflow
    mov byte[r12], al
    test rax, rax
    jz .check
    cmp rax, ` `
    je .check
    cmp rax, `\t`
    je .check
    cmp rax, `\n`
    je .check
    inc r12
    call read_char
    jmp .loop

 .overflow:
    mov rcx, r14
    dec rcx
    pop rdi
    mov rax, 0
    jmp .end

.check:
    mov rdx, r14
    dec rdx
    pop rax
 .end:
    pop r14
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
xor rax, rax
xor rdx, rdx
xor r10, r10
mov r11, 10

.loop:
    mov r10b, byte[rdi+rdx]
    cmp r10b, `0`
    jl .result
    cmp r10b, `9`
    jg .result
    push rdx
    mul r11
    pop rdx
    sub r10b, `0`
    add rax, r10
    inc rdx
    jmp .loop

.result:
    ret
	



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
xor rdx, rdx
cmp byte[rdi], '-'
je .negative
cmp byte[rdi], '+'
je .positive
call parse_uint
jmp .end
.negative: 
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .end
    inc rdx
    neg rax
    jmp .end
.positive:
    inc rdi
    call parse_uint
    cmp rdx, 0
    je .end
    inc rdx
.end:
    ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
xor rax, rax
push rbx
.loop:
    mov bl, byte[rdi+rax]
    mov byte[rsi+rax], bl
    dec rdx
    js .err
    cmp byte[rdi+rax], 0
    inc rax
    jne .loop
    jmp .end
.err:
     xor rax, rax

.end:
     pop rbx
     ret
